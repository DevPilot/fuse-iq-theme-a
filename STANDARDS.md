<!-- @file Instructions for FuseIQ theming standards. -->

Below are instructions and standards outlines for theming a Bootstrap
based subtheme on a Drupal Installation built and delivered by Fuse IQ

- [File Structure](#filestruture)
- [Naming conventions](#naming)
- [Template Files](#templates)
- [Miscellaneous](#misc)


## File Structure {#filestructure}
The only existing files that should be edited in the default installation
of the FuseIQ starter theme is
- `/scss/_default-variables.scss`
- `/scss/overrides.scss`
- `/scss/style.scss`

Any site specific theming shall go into a new folder appropriately titled such as
- `/scss/custom`

All files within this new directory shall roll up into one file such as
- `/scss/custom/custom_styles.scss`

The new file shall be included at the end of
- `/scss/style.scss`

Any bootstrap specific theming shall be overridden by creating overrides in the
new custom theming folder. This will prevent issues when updating the core
bootstrap files.

## Naming conventions {#naming}
NO Entity ID specific theming - Do NOT use NID, EID, TID, BID or any specific
identifier.
If Entity specific theming is required, use a body class or block class module

Try to adhere to a BEM standard that makes sense to the design.

[TODO] - Add more about naming and how items will be referenced

## Template Files {#templates}
NEVER use ID specific template files.

Only use template files when overall page markup absolutely has to change.

Keep PHP logic out of template files.

If logic needs to be applied, handle it outside the theme layer.

Avoid using too much logic in the {theme}.theme file as well.

Template files in project specific modules shall stay within the module. Only
contributed module template files shall be overridden within the theme/templates folder

Base template files such as 'page.html.twig' should be included first while 
setting up regions before major development starts.

Using a template file to alter output essentially caps future modifications at the
module level. Consider making preprocessing changes before the theming layer if possible.

## Miscellaneous {#misc}
[TODO] - Add other notes

Any custom local development tools MUST have an accompanying readme file. Not all
local environments will have the ability to perform automated tasks, so a manual
process must be available to compile stylesheets.

## Secondary CSS files
This functionality will allow a second set of css files to be used to prevent the majority of the
existing/core theme CSS from being fouled up.

To compile, from the base theme directory run from the command line:
- sass scss/secondary.scss css/secondary.css

This new CSS file is included as a separate line item in the libraries file for the theme.