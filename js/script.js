(function($) {

      Drupal.behaviors.fuseiq_custom = {
        attach: function (context, settings) {
          if (context == document) {
            
            // hover to change what is shown in grid and staggered callouts.  Still need to add svg and other 
            // color changes
            // $(".paragraph--type--grid-callout, .paragraph--type--staggered-callout").hover(function(){
            //   $(this).find(".field--name-field-hover-text").toggle();
            //   $(this).find(".field--name-field-body").toggle();
            // });
            
            // init Isotope
/*
            var $grid = $('.isotope-grid').isotope({
              itemSelector: '.isotopes-grid-item',
              layoutMode: 'fitRows',
                masonry: {
                  columnWidth: '.grid-sizer'
                }
            });
*/
            var $grid = $('.isotope-grid').imagesLoaded( function() {
              // init Isotope after all images have loaded
              $grid.isotope({
              itemSelector: '.isotopes-grid-item',
              layoutMode: 'fitRows',
                masonry: {
                  columnWidth: '.grid-sizer'
                }
              });
            });
            
            // bind filter button click
            $('#industry-options-form').on( 'click', '.form-radio', function() {
              var filterValue = $( this ).attr('value');
              if(filterValue == '*') {
                $grid.isotope({ filter: filterValue });
              } else {
                $grid.isotope({ filter: '.' + filterValue });
              }
            });            

          }
        }
      };

	


})(jQuery);

